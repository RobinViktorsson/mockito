package mockito.tests.injectmocks;

public interface Service {
    public boolean send(String msg);
}

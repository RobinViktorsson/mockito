/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mockito.tests;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class Test002_SpyTest {
    
    @Spy       
    List<String> spyOnList = new ArrayList();

    @Before
    public void setup() {
        // Requirement if we gonna use mock annotations
        MockitoAnnotations.initMocks(this);
    }

    /**
     * This method makes use of @Spy annotation.
     */
    @Test
    public void test001() { 
        // We insert an element into the spyOnList
        spyOnList.add("a");
        
        // Ensure that the size is 1
        assertEquals(1, spyOnList.size());
        
        // Check that the first element contains "a"
        assertEquals("a", spyOnList.get(0));

        // Add another element into to list and perform the same checks
        spyOnList.add("b");
        assertEquals(2, spyOnList.size());
        assertEquals("b", spyOnList.get(1));

        // Control list size
        when(spyOnList.size()).thenReturn(3);
        assertEquals(3, spyOnList.size());
    }
    
    /**
     * This methods make use of the spy-method.
     * This does not require us to invoke MockAnnotations.initMocks(this);
     * It has the equivalent effect as the first test method.
     */
    @Test
    public void test002() {
        List<String> list2 = new ArrayList();
        List<String> spyOnList2 = spy(list2);
		
        spyOnList2.add("a");
        assertEquals(1, spyOnList2.size());
        assertEquals("a", spyOnList2.get(0));

        spyOnList2.add("b");
        assertEquals(2, spyOnList2.size());
        assertEquals("b", spyOnList2.get(1));

        when(spyOnList2.size()).thenReturn(3);
        assertEquals(3, spyOnList2.size());
    }
}

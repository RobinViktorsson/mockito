/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mockito.tests;

import java.util.List;

import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Test001_MockTest {
    
    @Mock
    List<String> mockList;
    
    @Before
    public void setup() {
        // Requirement if we gonna use mock annotations
        MockitoAnnotations.initMocks(this);
    }
    
    /**
     * This method makes use of @Mock annotation.
     */
    @Test
    public void test001() {
        // When we get 0, it should return the string FirstElement
        when(mockList.get(0)).thenReturn("FirstElement");
        
        // Let's make use of assertEquals to ensure that happens!
        assertEquals("FirstElement", mockList.get(0));
    }
    
    /**
     * This methods make use of the mock-method.
     * This does not require us to invoke MockAnnotations.initMocks(this);
     * It has the equivalent effect as the first test method.
     */
    @Test
    public void test002() {
        // using Mockito.mock() method
        List<String> mockList = mock(List.class);
        
        // When we call mockList.size() method we want the mock object to return 1
        when(mockList.size()).thenReturn(1);
        
        // We make use of assertTrue to ensure that happens.
        assertTrue(mockList.size()==1);
    }
}

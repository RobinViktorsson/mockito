package mockito.tests;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.verify;

public class Test004_ArgumentCaptorTest {
    
    @Mock
    List mockedList;
    
    @Captor
    ArgumentCaptor argCaptor;
    
    @Before
    public void setup() {
        // Requirement if we gonna use mock annotations
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test001() {
        // Add string to List
        mockedList.add("a string argument");
        
        // Capture the argument
        verify(mockedList).add(argCaptor.capture());
        
        // Get the captured argument and compare it using assertEquals
        assertEquals("a string argument", argCaptor.getValue());
    }
}

package mockito.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyString;

import mockito.tests.injectmocks.AppServices;
import mockito.tests.injectmocks.AppServices1;
import mockito.tests.injectmocks.AppServices2;
import mockito.tests.injectmocks.EmailService;
import mockito.tests.injectmocks.SMSService;

public class Test003_InjectMocksTest {
    
    @Mock 
    EmailService emailService;

    @Mock 
    SMSService smsService;

    @InjectMocks 
    AppServices appServicesConstructorInjectionMock;

    @InjectMocks 
    AppServices1 appServicesSetterInjectionMock;

    @InjectMocks 
    AppServices2 appServicesFieldInjectionMock;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }
    
    /**
     * Constructor based injection.
     */
    @Test
    public void test001_ConstructorInjectionMock() {
        when(appServicesConstructorInjectionMock.sendEmail("Email")).thenReturn(true);
        when(appServicesConstructorInjectionMock.sendSMS(anyString())).thenReturn(true);

        assertTrue(appServicesConstructorInjectionMock.sendEmail("Email"));
        assertFalse(appServicesConstructorInjectionMock.sendEmail("Unstubbed Email"));
        assertTrue(appServicesConstructorInjectionMock.sendSMS("SMS"));
    }
    
    /**
     * Setter method based injection
     */
    @Test
    public void test002_SetterInjectionMock() {
        //Setters
        when(appServicesSetterInjectionMock.sendEmail("New Email")).thenReturn(true);
	when(appServicesSetterInjectionMock.sendSMS(anyString())).thenReturn(true);
	
	assertTrue(appServicesSetterInjectionMock.sendEmail("New Email"));
	assertFalse(appServicesSetterInjectionMock.sendEmail("Unstubbed Email"));
	assertTrue(appServicesSetterInjectionMock.sendSMS("SMS"));
    }
    
    /**
     * Field based injection
     */
    @Test
    public void test003_FieldInjectionMock() {
        when(appServicesFieldInjectionMock.sendEmail(anyString())).thenReturn(true);
	when(appServicesFieldInjectionMock.sendSMS(anyString())).thenReturn(true);
	
	assertTrue(appServicesFieldInjectionMock.sendEmail("Email"));
	assertTrue(appServicesFieldInjectionMock.sendEmail("New Email"));
	assertTrue(appServicesFieldInjectionMock.sendSMS("SMS"));
    }
}

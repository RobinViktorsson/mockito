/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mockito.tests;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import static org.mockito.AdditionalMatchers.aryEq;
import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class Test005_ArgumentMatchersTest {
    
    @Test
    public void test001() {   
        // Create mock object
        TestClass test = mock(TestClass.class);

        // Static argument matchers
        when(test.bool(anyString(), anyInt(), any(Object.class))).thenReturn(true);
        assertTrue(test.bool("a", 1, "a"));
        assertTrue(test.bool("b", 2, new Object()));
        assertTrue(test.bool("false", 2, new Object()));

        // Conditional argument matchers
        when(test.bool(eq("false"), anyInt(), any(Object.class))).thenReturn(false);
        assertFalse(test.bool("false", 2, new Object()));
        when(test.in(anyBoolean(), anyList())).thenReturn(2);

        // Arrays and additional matchers
        when(test.test(any(byte[].class), aryEq(new String[] { "a", "b" }), gt(2))).thenReturn(3);
        assertEquals(3, test.test("abc".getBytes(), new String[] { "a", "b" }, 4));
        assertEquals(3, test.test("xyz".getBytes(), new String[] { "a", "b" }, 5));

        // Argument matcher combined with verify
        verify(test, atLeast(0)).bool(anyString(), anyInt(), any(Object.class));
        verify(test, atLeast(0)).bool(eq("false"), anyInt(), any(Object.class));
    }
}

class TestClass {
    boolean bool(String str, int i, Object obj) { return false; }
    int in(boolean b, List<String> strs) { return 0; }
    int test(byte[] bytes, String[] s, int i) { return 0; }
}
